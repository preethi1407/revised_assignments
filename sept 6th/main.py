def sub(x,y):
        '''
        This function is used to subtract two numbers.

        Parameters
        ----------
        x : int
            the number from which other number is to be subtracted
            
        y : int
            the number that is to be subtracted

        Returns
        -------
            int: subtracted result of two numbers
        '''
        return x-y