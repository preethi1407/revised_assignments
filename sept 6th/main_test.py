import unittest
import main
class test_subtract(unittest.TestCase):
        '''
        A class to test all the assert functionalities

        Methods
        -------
        test_sub(self):
            this method is used to check the assertEqual functionality

        test_sub_notequal(self):
            this method is used to check the assertNotEqual functionality

        test_sub_True(self):
            this method is used to check the assertTrue functionality

        test_sub_False(self):
            this method is used to check the assertFalse functionality

        test_sub_Is(self):
            this method is used to check the assertIs functionality

        test_sub_IsNot(self):
            this method is used to check the assertIsNot functionality

        test_sub_In(self):
            this method is used to check the assertIn functionality

        test_sub_almostEqual(self):
            this method is used to check the assertAlmostEqual functionality

        test_sub_almostnotEqual(self):
            this method is used to check the assertNotAlmostEqual functionality


        test_sub_greater(self):
            this method is used to check the assertGreater functionality

        '''

        def test_sub(self):
            '''this method is used to check assertEqual()functionality that is used in unit testing to check the equality of two values'''
            self.assertEqual(main.sub(15,10),5)

        def test_sub_notequal(self):
            '''this method is used to check assertNotEqual () functionality that is used in unit testing to check the inequality of two values.'''
            self.assertNotEqual(main.sub(15,10),6)

        def test_sub_True(self):
            '''this method is used to check assertTrue () functionality in Python that is used in unit testing to compare test value with true'''
            self.assertTrue(main.sub(15,15)==0,"the result is false")

        def test_sub_False(self):
            '''this method is used to check assertTrue () functionality in Python that is used in unit testing to compare test value with false'''
            self.assertFalse(main.sub(10,15)==2,"the result is true")

        def test_sub_Is(self):
            '''this method is used to check assertIs () functionality in Python that is used in unit testing to test whether first and second input value evaluates to the same object or not.'''
            self.assertIs(main.sub(15,10),main.sub(15,10),"does not match")
        
        def test_sub_IsNot(self):
            '''this method is used to check assertIsNot () functionality in Python that is used in unit testing to test whether first and second input value don’t evaluate to the same object or not. '''
            self.assertIsNot(main.add(10,15),main.add(10,55),"match")

        def test_sub_In(self):
            '''this method is used to check assertIn () functionality in Python that is used in unit testing to check whether a string is contained in other or not. '''
            self.assertIn(main.sub(15,15),[0,5,7],"not in list")

        def test_sub_almostEqual(self):
            '''this method is used to check assertAlmostEqual () functionality in Python that is used in unit testing to check whether two given values are almost equal or not.'''
            self.assertAlmostEqual(main.sub(40,15),25.000000001)

        def test_sub_almostnotEqual(self):
            '''this method is used to check assertNotAlmostEqual () functionality in Python that is used in unit testing to check whether two given values are not approximately.'''
            self.assertNotAlmostEqual(main.sub(40,15),25.0985001)

        def test_sub_greater(self):
            '''this method is used to check assertGreater () in Python that is used in unit testing to check whether the first given value is greater than the second value or not.'''
            self.assertGreater(main.sub(15,10),1)

if __name__=='__main__':
    unittest.main()