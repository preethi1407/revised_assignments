'''
Object oriented program to implement a Random Password Generator using Python
The pass_gen class contains the following member functions.
 -> password_generator
 -> printStrongNess

'''

#import the necessary modules!
import random
import string

## characters to generate password from
letters = list(string.ascii_letters)
numbers = list(string.digits)
special_characters = list("!@#$%^&*()")
characters = list(string.ascii_letters + string.digits + "!@#$%^&*()")

class pass_gen:
    '''
        Creates a random password and is used to check the strength of the password

        Methods
        -------

        password_generator(self):
            this method is used to generate a random password of specified features say alphabet_count,digits_count,special_characters_count

        printStrongNess(self,*args):
            this function is used to check the strenth of the generated password
        '''
    def password_generator(self): 
        '''
        this method is used to generate a random password of specified features say alphabet_count,digits_count,special_characters_count
        '''
        #taking the length of password from the user
        length = int(input("Enter password length: "))

        ## taking the count of alphabets,digits and special characters to be included in the password
        alphabets_count = int(input("Enter alphabets count in password: "))
        digits_count = int(input("Enter digits count in password: "))
        special_characters_count = int(input("Enter special characters count in password: "))

        characters_count = alphabets_count + digits_count + special_characters_count

        ## check the total length with characters sum count print not valid if the sum is greater than length
        if characters_count > length:
            print("Characters total count is greater than the password length")
            return


        # creating an empty list for password
        password = []
        
        # picking random alphabets
        for i in range(alphabets_count):
            password.append(random.choice(letters))


        # picking random digits
        for i in range(digits_count):
            password.append(random.choice(numbers))


        # picking random alphabets
        for i in range(special_characters_count):
            password.append(random.choice(special_characters))


        ## if the total characters count is less than the password length add random characters to make it equal to the length
        if characters_count < length:
            random.shuffle(characters)
            for i in range(length - characters_count):
                password.append(random.choice(characters))


        #shuffling the resultant password
        random.shuffle(password)

        #converting the list to string printing the list
        print("New generated password with given parameters is : "+"".join(password))
            
    def printStrongNess(self,*args):
        '''this function is used to check the strenth of the generated password
        Parameters
        ----------
            *args : string
                the random password to which the strength is to be verified'''
        for x in args:
            n = len(x)

            # Checking lower alphabet in string
            hasLower = False
            hasUpper = False
            hasDigit = False
            specialChar = False
            normalChars = "abcdefghijklmnopqrstu"
            "vwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 "
            
            for i in range(n):
                if x[i].islower():
                    hasLower = True
                if x[i].isupper():
                    hasUpper = True
                if x[i].isdigit():
                    hasDigit = True
                if x[i] not in normalChars:
                    specialChar = True
        
            # Strength of password
            print("Strength of password for "+x+"is : ", end = "")
            # checking for the conditions to satisfy the strong condition of a password
            if (hasLower and hasUpper and
                hasDigit and specialChar and n >= 8):
                print("Strong")

            # # checking for the conditions to satisfy the moderate condition of a password 
            elif ((hasLower or hasUpper) and
                specialChar and n >= 6):
                print("Moderate")
            else:
                print("Weak")

# driver code
if __name__=="__main__":
    res = pass_gen()
    res.password_generator()
    res.printStrongNess("Preethi@2000")

