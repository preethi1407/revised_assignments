''' This program is a Python3 code to Check for balanced parentheses in an data read from the file
This program Contains the function is_balanced(input1) which is used to check if the parentheses are balanced in a file
 
'''
# initializing open and closed set of parantheses
open_braces = ["[", "{", "("]
closed_braces = ["]", "}", ")"]


def is_balanced(input1):
    ''' Function to check parentheses
    Parameters
        ----------
        input1 : str
            string of data for which the is_balanced() function is to be applied
        Returns
        -------
            Balanced: If there are balanced number of parantheses in the content
            Unbalanced: If there are unbalanced number of parantheses in the content
 '''
#  taking an empty list to append the 'open_braces' and 'closed_braces' in the content read
    stack = []
# checking for the braces in the content read
    for i in input1:
        if i in open_braces:
# appending the braces found in the content read to the empty stack
            stack.append(i)
        elif i in closed_braces:
            pos = closed_braces.index(i)
# checking if the open_braces and the stack have the same braces and popping them off the stack if they are same else returning that they are unbalanced
            if ((len(stack) > 0) and
                    (open_braces[pos] == stack[len(stack)-1])):
                stack.pop()
            else:
                return "Unbalanced"
# checking the length of the stack and returning Balanaced if its length is 0 or else returning Unbalanced
    if len(stack) == 0:
        return "Balanced"
    return "Unbalanced"

# reading a file to check whether the content has balanced number of paranthese or not
filename = input("Enter the file name: ")
# opening a file in read mode and storing all the data in the read file into a variable data
with open(filename, "r", encoding="utf-8") as text_file:
    data = text_file.read()
    text_file.close()

# converting all the data into a string that is to be passed as an argument to the is_balanced() function
input2 = str(data)
# checking if the content is balanced or not using the is_balanced() function
print(input2, "-", is_balanced(input2))
